import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, of } from 'rxjs';
import { AppService } from './app.service';
import { ToDo } from './app.type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  array = [0, 1, 2];

  observable$ = of(true);

  numbers = 5;

  objects = { test: 'test' };

  labelText = 'test';

  private toDo$$ = new BehaviorSubject<ToDo | null>(null);
  toDo$ = this.toDo$$.asObservable();

  constructor(private testService: AppService) {}

  ngOnInit(): void {
    this.testService.search().subscribe((value) => this.toDo$$.next(value));
  }
}
