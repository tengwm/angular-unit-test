import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { AppService } from './app.service';

import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { ToDo } from './app.type';

describe('AppService', () => {
  let service: AppService;
  let mockHttp: HttpTestingController;
  let mockResponse: ToDo;
  let url: string;
  let mockError: ErrorEvent;
  let error: Notification;
  const message = 'An error occurred';

  beforeEach(() => {
    url = 'https://jsonplaceholder.typicode.com/todos/1';
    mockResponse = {
      id: 1,
      userId: 1,
      completed: false,
      title: 'test',
    };
    mockError = new ErrorEvent('Network error', {
      error,
      message,
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService],
    });
    service = TestBed.inject(AppService);
    mockHttp = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call service and return approriate respond', () => {
    service.search().subscribe((res) => expect(res).toEqual(mockResponse));

    const req = mockHttp.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('should throw error when there is error', () => {
    service.search().subscribe({
      error: (err: HttpErrorResponse) =>
        expect(err.error.message).toBe(mockError.message),
    });

    const req = mockHttp.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(mockError, { status: 400, statusText: 'Bad Request' });
  });
});
