import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ToDo } from './app.type';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private http: HttpClient) {}

  search(): Observable<ToDo> {
    return this.http
      .get<ToDo>('https://jsonplaceholder.typicode.com/todos/1')
      .pipe(
        catchError((err) => {
          throw err;
        })
      );
  }
}
