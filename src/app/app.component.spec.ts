import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { of } from 'rxjs';
import { ToDo } from './app.type';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let mockAppService: jasmine.SpyObj<AppService>;

  beforeEach(waitForAsync(() => {
    mockAppService = jasmine.createSpyObj<AppService>('mockAppService', [
      'search',
    ]);
    mockAppService.search.and.returnValue(of({} as ToDo));

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AppService,
          useValue: mockAppService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should check array value', () => {
    expect(component.array).toContain(1);
  });

  it('should check observable value', fakeAsync(() => {
    component.observable$.subscribe((value) => expect(value).toBeTrue());
  }));

  it('should check numbers value', () => {
    expect(component.numbers).toBe(5);
  });

  it('should check objects value', () => {
    const mockTest = { test: 'test' };

    expect(component.objects).toEqual(mockTest);
  });

  it('should check testLabel html', () => {
    const testLabelValue =
      fixture.debugElement.nativeElement.querySelector('#testLabel');

    expect(testLabelValue.innerHTML).toBe('test');
  });

  it('should check service is called', () => {
    component.ngOnInit();

    expect(mockAppService.search).toHaveBeenCalledWith();
  });
});
